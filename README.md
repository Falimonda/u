# u(tilities)

The `u` CLI tool allows developers to more effectively work with git on a multi-repo application or system. 

:warning: **This utility will define a collection of short git aliases you can use on individual repos if you so choose. If you don't want those git shortcut aliases loaded into your env simply export U_NO_GIT_SHORTCUTS=1 into your env. Contributions are encouraged! Open an MR with additional shortcuts that you find to be intuitive and useful.**

---

 ## :warning: TERMS OF USE :warning:

By using this tool you agree to the following terms.

1. You are solely responsible for the contents of configs/repos.json, configs/.env, and the use of any and all parts of this utility. 

2. The author and contributors of this tool assume no responsibilty for the outright destruction of your local and/or remote repositories due to your misunderstanding or misuse of this tool and/or git. 

3. No guarantees are made now nor in perpituity as to the current capabilities, correctness, safety, nor completeness of the tool and how it may or may not affect your local and/or remote repos. 

You have been warned. 

Turn back now if you do not agree to these terms. If you do continue, you are encouraged to read through this README.md, the scripts contained, and any prompts shown while using the utility to carefully understand what it does.

---

## Setup

1. Modify the provided [.env](configs/.env) and [repos.json](configs/repos.json) templates to match your personal setup. Check below for more details on these files.
2. Source the scripts/utilities.sh file in your preferred terminal profile (~/.bashrc, ~/.profile, etc.) if you want to make it available in your terminal every time you open a new one. Source it once in an existing terminal if you just want to mess around with it first.
3. Call `u clone` to clone any new repos enabled in [repos.json](configs/repos.json) into $U_REPOS_ROOT.
4. Test out the utility by running `u do git status`.

---

## Example Use Cases


Assumptions:

1. A number of repos (N) are listed and set `enabled` attribute true in [repos.json](configs/repos.json)
2. Those repos are currently cloned in $U_REPOS_ROOT.
3. Your workspace in each of the N repos is CLEAN and you have NO UNCOMMITED WORK YOU WANT TO RISK LOSING.

### Case #1: Batch Git Status

You: "I want to know the git status of all my N local repos without `cd`ing into each and running the same commands over and over again."

Commands: 

    u do git status

### Case #2: Batch Addressing a Multi-repo Ticket

You: "My ticket involves the same or a similar change across N of my repos. I want to work effectively, use the same branch name across all my repos, etc. etc. etc."

Commands:

    # checkout the main branch and pull latest changes - this will prompt you to hard reset if you have uncommitted work (!!!)
    u update main

    # create a new branch with the same name
    u do git checkout -b TICKET-123-same-change

    # make your desired file changes; the following line is just an example using the nano text editor to modify a file that exists in all N imaginary repos referred to in this example
    u do nano config.json

    # add the changes to staging
    u do git add config.json

    # commit the changes, using the same commit message across all N repos
    u do git commit -m "changes something or other"

    # push changes to remote, tracking upstream
    u do git push -u origin TICKET-123-same-change

---

## [configs/.env](configs/.env)

Environment variables used by this utility which you must customize for your own system. Check the provided [.env](configs/.env) template for descriptions of each variable.

---

## [configs/repos.json](configs/repos.json)

A JSON list of the repos that `u` manages along with attributes that determine the behavior of specific subcommands.

Entry attributes: 

- `name` : string - the name of the repository; must also correspond to the local repo's directory name found in $U_REPOS_ROOT.
- `type` : string - a name for the purpose or service that the repository provides within your system - typically you will have multiple repos of the same type such as `service` or `docs`. At this time, the value is unused except for in the `list_by_type` helper function. In the future, this attribute might be used to control how the utility behaves for a given repo type within the context of your system.
- `enabled` : boolean - whether the `u`'s batch operations will operate on this repository or not.

You may wish to self-reference the `u` repository within the configs/repos.json but doing so is currently untested. Is is therefore recommended that you manually keep it up to date.

---

## Scripts Files

[clone.sh](scripts/clone.sh) - clones the enabled repos into $U_REPOS_ROOT, skips if already cloned

[do.sh](scripts/do.sh) - performs a command on all repos that are marked `enabled` in [configs/repos.json](configs/repos.json) - check [do.sh](scripts/do.sh) for additional usage information.

[git_shortcuts.sh](scripts/git_shortcuts.sh) - quick git aliases and functions for use on any individual repo

[helpers.sh](scripts/helpers.sh) - common helper functions that can be used by utilities

[status.sh](scripts/status.sh) - called via `u status` - prints git status along with additional customized info of all enabled repos

[update.sh](scripts/update.sh) - called via `u update {BRANCH_NAME}` - updates the targeted repo's specified branch by attempting to check out the target branch, prompting to do a hard reset if uncommited or unstaged changes exist in the current branch, and then pulling changes on the target branch from remote if checkout was successful

[utilities.sh](scripts/utilities.sh) - the file you should source in your ~/.bashrc or desired profile to make use of `u` and the provided git shortcuts

[vars.sh](scripts/vars.sh) - provides env variable checking and printing

# Subcommands

## Non-batch Commands

Non-batch commands are used to query or interact with the utility or your system as a whole (not on any repo like batch commands).

### env

Prints the env variables that are being used by `u` in the current shell.

### root

Changes directory to the current value of `$U_REPOS_ROOT`.

## Batch Commands

Batch commands act on all repos defined in [configs/repos.json](configs/repos.json) with their `enabled` attribute set to true.

### do

Executes the strings following `u do` as a command within each enabled repo.

Example: `u do git add -A`

### clone

Clones all the enabled repos into `$U_REPOS_ROOT` unless the repo already exsists. 

Usage: `u clone`

Great for initializing the `u` tool on a new project or new repo. Simply add your repo definition within [repos.json](configs/repos.json) and run `u clone`. 

### status

Prints `git status` output for all enabled repos along with some additional information such as latest commit ID.

Usage: `u status`

### update

Attempts to pull updates to the target branch for all enabled repos.

Example: `u update main` 

If `update` finds unstaged changes it will prompt the user to decide whether the utility should hard reset (!!!) or skip updating that repo.

### enabled

Prints a list of currently enabled repos.

Usage: `u enabled`

### disabled

Prints a list of currently disabled repos.

Usage: `u disabled`
