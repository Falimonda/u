1. Support unstaged changes handling policy flags to `u update` command. At the moment --hard reset is the assumed, desired policy.
2. Add tests for file and function names to trigger on git commit hook
3. Add support for pipes and redirects in 'do' subcommand
