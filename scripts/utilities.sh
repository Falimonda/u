#!/usr/bin/env bash

SCRIPTS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
UTILS="${SCRIPTS_DIR}/.."
CONFIGS_DIR="${UTILS}/configs"

source $CONFIGS_DIR/.env
source $SCRIPTS_DIR/vars.sh && vars_check
source $SCRIPTS_DIR/helpers.sh
if [[ $U_NO_GIT_SHORTCUTS -ne 1 ]]; then
    source $SCRIPTS_DIR/git_shortcuts.sh
fi

# main entry point for utility sub commands including batch actions across any enabled repos in configs/repos.json
function u(){
    ## batch subcommands
    if [[ $1 == "do" ]];then
        $UTILS/scripts/do.sh "${@:2}"
    elif [[ $1 == "clone" ]];then
        $UTILS/scripts/clone.sh
    elif [[ $1 == "status" ]];then
        $UTILS/scripts/status.sh
    elif [[ $1 == "update" ]];then
        $UTILS/scripts/update.sh $2
    elif [[ $1 == "enabled" ]];then
        repos=$(list_enabled)
        echo "${repos[@]}"
    elif [[ $1 == "disabled" ]];then
        repos=$(list_disabled)
        echo "${repos[@]}"
    ## non-batch sub commands 
    elif [[ $1 == "env" ]];then
        vars_check show
    elif [[ $1 == "root" ]];then
        cd $U_REPOS_ROOT
    elif [[ $1 == "edit" ]];then
		command=$2
		if [[ $command == "git" ]];then
			eval $U_EDITOR $SCRIPTS_DIR/git_shortcuts.sh
		elif [[ $command == "configs" ]];then
			subcommand=$3
			if [[ $subcommand == "repos" ]];then
				eval $U_EDITOR $CONFIGS_DIR/repos.json
			elif [[ $subcommand == "env" ]]; then
				eval $U_EDITOR $CONFIGS_DIR/.env
			else
				echo "ERROR: Invalid command - $command $subcommand"
			fi
		elif [[ $command == "all" ]];then
			eval $U_EDITOR $SCRIPTS_DIR/*
		elif [[ ! -z $command ]];then
			command_file="$SCRIPTS_DIR/$command.sh"
			if [[ ! -e $command_file ]];then
				echo "ERROR: Could not find a match for $command_file"
				return 1
			fi
			eval $U_EDITOR $command_file
		else
			eval $U_EDITOR $SCRIPTS_DIR/utilities.sh
		fi
    fi

}
