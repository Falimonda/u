#!/usr/bin/env bash

SCRIPTS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

source $SCRIPTS_DIR/vars.sh && vars_check
source $SCRIPTS_DIR/helpers.sh

repos=$(list_enabled)
if [[ -z "$repos" ]];then
    echo "ERROR: enabled repos could not be loaded. Is the configs/repos.json file missing?"
    exit 1
fi

BRANCH_INPUT=$1

if [[ -z "$BRANCH_INPUT" ]];then
    echo "Usage: ./update.sh {BRANCH_NAME}"
    echo "Exiting due to missing branch name input."
    exit 1
fi

function update_repo_branch(){
    echo
    REPO=$1
    BRANCH=$2
    print_batch_header "Updating $REPO"
    cd $U_REPOS_ROOT/$REPO
    if [[ $? -eq 0 ]];then
		echo "Fetching changes..."
        git fetch 2>&1 > /dev/null
		echo "Verifying branch name exists..."
		BRANCH_EXISTS=$(git ls-remote --heads $GIT_SERVER:$GIT_ORGANIZATION/$REPO.git $BRANCH)
        if [[ -z "$BRANCH_EXISTS" ]];then
            echo "ERROR: $BRANCH branch in $REPO does not exist."
            return 1
        fi
		echo "Checking out branch..."
        git checkout $BRANCH 2>&1 > /dev/null
        if [[ ! -z "$(git status --porcelain)" ]];then
            git status
            echo 
            echo "Would you like to destroy UNCOMMITED, UNSTAGED, or UNTRACKED CHANGES ON HEAD PERMANENTLY, or skip updating this repo?"
            echo "WARNING: Entering 'destroy' will discard ALL uncommited changes and files. You may lose hours or days of work if you are not careful. You've been warned!"
            res=1
            while [[ $res -ne 0 ]];do
                read -p "Are you sure it's OK for me to hard reset ~/git/$REPO? [destroy/skip] " response
                if [[ "$response" == "destroy" ]] || [[ "$response" == "skip" ]];then
                    res=0
                else
                    echo "Try again. Valid options are 'destroy' or 'skip'"
                fi
            done
            if [[ "$response" == "destroy" ]];then
                : #continuing
            else
                echo "Did not hard reset nor update this repo."
                return 1
            fi
            git reset --hard HEAD
            git clean -df
            git checkout $BRANCH 2>&1 > /dev/null
            if [[ "$?" -ne 0 ]];then
                echo "Error: While updating, even after resetting and cleaning the current working directory I could not checkout your desired branch: $BRANCH. You will need to manually resolve the issue to proceed. Exiting."
                exit 1
            fi
        fi
        git pull 2>&1 > /dev/null
        if [[ ! $? -eq 0 ]]; then
            echo "ERROR: Could not pull development changes in ~/git/$REPO."
            exit 1
        fi
        cd - 2>&1 > /dev/null
    else
        echo "The repo does not exist on this machine. Skipping."
    fi
}

for repo in $(echo $repos); do
    update_repo_branch $repo $BRANCH_INPUT
done

