#!/usr/bin/env bash

# aliases and functions for use on an individual repo

##### add current git branch in shell prompt if current dir is a repo
parse_git_branch() {
     git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
}
export PS1="\u@\h \[\033[32m\]\w\[\033[33m\]\$(parse_git_branch)\[\033[00m\] $ "

##### define some useful git aliases

### branching / checkout
alias gb='git branch'
# git checkout and branch
function gcb(){
    git checkout -b $1
}
# git checkout specific branch
function gc(){
    git checkout $1
}

### fetching
alias gf='git fetch --prune'

### adding
alias gaa='git add -A'
alias gai='git add -i'
alias gau='git add -u'
function ga(){
	git add ${@}
}

### committing
alias gca='git commit -a'

### rebasing
alias grc='git rebase --continue'

### diffing
function gd(){
	git diff ${@}
}
alias gdc='git diff --cached'

### logging
alias glp='git log -p'

### status..ing
alias gs='git status'

# git delete all local merged branches
## this is a cleaner to keep your local git repo free of branches are are likely no longer necessary
function gdl(){
    BASE_BRANCH=$1
    if [[ -z $BASE_BRANCH ]];then
        echo "ERROR: No base branch argument provided."
        return 1
    fi
    read -p "Are you sure you want to delete all local branches that have been merged into $BASE_BRANCH? [Y/y] " CHOICE
    if [[ $CHOICE != "y" ]] && [[  $CHOICE != "Y" ]]; then
        echo "Exiting without continuing."
        return 0
    fi
    git branch --merged $BASE_BRANCH | grep -v "^\* $BASE_BRANCH" | xargs -n 1 -r git branch -d
}

# git push branch to origin and track upstream if new
function gpush(){
    CURRENT_BRANCH=$(git rev-parse --abbrev-ref HEAD)
    if [[ -z $CURRENT_BRANCH ]];then
        echo "Failed to get current branch. Is the current directory a git repo?"
    fi
    if [[ $CURRENT_BRANCH == "main" ]];then
        echo "This alias cannot act on main branch. Exiting without pushing."
        return 1
    fi
    if [[ $CURRENT_BRANCH == "development" ]];then
        echo "This alias cannot act on development branch. Exiting without pushing."
        return 1
    fi
    UPSTREAM=$(git for-each-ref --format='%(upstream:short)' "$(git symbolic-ref -q HEAD)")
    if [[ -z $UPSTREAM ]];then
        git push -u origin $CURRENT_BRANCH
    else
        git push
    fi
}

# visualize a repo's history
alias logadog='git log --all --decorate --oneline --graph --tags'
alias gdog='logadog'
