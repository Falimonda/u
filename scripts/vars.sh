#!/usr/bin/env bash

function check_var(){
# check whether the variable provided is a variable in the current env

	INPUT=$1
	if [[ -z $INPUT ]];then
		echo "ERROR: check_var received no input argument. It expects a variable name whose existence will be checked in the current env."
	fi

	if [[ -z ${!INPUT} ]];then
		echo "ERROR: The ${INPUT} var is missing from your env. It is suggested that you define it in configs/.env"
		return 1
	fi

	if [[ ! -z $SHOW ]]; then
		echo $INPUT : ${!INPUT}
	fi
}

function vars_check(){
	SHOW=$1
	check_var "U_REPOS_ROOT" $SHOW
	check_var "GIT_ORGANIZATION" $SHOW
	check_var "GIT_DOMAIN" $SHOW
	check_var "GIT_SERVER" $SHOW
}
