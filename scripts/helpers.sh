#!/usr/bin/env bash

SCRIPTS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
CONFIGS_DIR="$SCRIPTS_DIR/../configs"

# returns a list of the repos that are enabled for batch actions
function list_enabled(){
    cat $CONFIGS_DIR/repos.json | jq -r '.[] | select(.enabled == true) | .name' | tr $'\r' ' '
}

# returns a list of the repos that are NOT enabled for batch actions
function list_disabled(){
    cat $CONFIGS_DIR/repos.json | jq -r '.[] | select(.enabled == false) | .name' | tr $'\r' ' '
}

# returns a list of repos that match the desired service type name
function list_by_type(){
    cat $CONFIGS_DIR/repos.json | jq -r ".[] | select(.type==\"$1\") | .name" | tr $'\r' ' '
}

function get_repo_url(){
    REPO=$1
    if [[ -z $REPO ]];then
        return 1
    fi

    if [[ $GIT_PROTOCOL == "ssh" ]]; then
        echo "${GIT_SERVER}:${GIT_ORGANIZATION}/${REPO}.git"
    elif [[ $GIT_PROTOCOL == "http" ]]; then
        echo "https://${GIT_DOMAIN}/${GIT_ORGANIZATION}/${REPO}.git"
    else
        return 1
    fi
}

function print_batch_header(){
        HEADER="${@}"
        echo
        echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
        echo "$HEADER"
        echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
}

repos=$(list_enabled | tr -d $'\n' | tr -d ' ')
if [[ -z $repos ]];then
    echo
    echo "WARNING: the 'u' utility detected no named, enabled repos in configs/repos.json. This utility won't be of much use without enabled repos defined."
    echo
fi
