#!/usr/bin/env bash

SCRIPTS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source $SCRIPTS_DIR/helpers.sh

cd $U_REPOS_ROOT
echo 1
repos=$(list_enabled)
echo 2
for repo in $(echo $repos); do
   echo 3
   print_batch_header "Cloning $repo" 
   repo_url=$(get_repo_url $repo)
   git clone $repo_url
done

cd -
