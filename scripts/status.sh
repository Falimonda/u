#!/usr/bin/env bash

SCRIPTS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source $SCRIPTS_DIR/helpers.sh

function get_id(){
    COMMIT_ID=$(git log -1 | grep 'commit' | awk '{print $2}')
    echo "Current git commit ID: ${COMMIT_ID}"
}

function show_status(){
    if [[ -z $1 ]];then
        echo "Nothing to do here. Exiting."
        exit 1
    fi
    print_batch_header "Showing ${1} status"
    cd $U_REPOS_ROOT/${1} 2>&1 > /dev/null
    if [[ $? -eq 0 ]];then
        git fetch
        git status
        get_id
    else
        echo "The repo does not exist on this machine. Skipping."
    fi
}

repos=$(list_enabled)
for repo in $(echo $repos); do
   show_status $repo
done
