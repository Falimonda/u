#!/usr/bin/env bash

usage="Usage: $(basename "$0") [-h] [-i a,b,c,...] -- [command] [args]

   This script executes whatever command with arguments are passed to it after
   cd-ing into each enabled repo directory. The array of repos and their attributes 
   is loaded from configs/repos.json

where:
    -h      show this help text
    -i      skip the comma-separated list of repos
    a,b,c   comma separated list of repos

Examples:

do ls
    - lists directory contents

do -i utils,infrastructure -- git checkout -b feature-abc
    - creates a new branch in all repo dirs except for utils and infrastructure if those are currently enabled

do git add -A
    - adds all unstaged changes and untracked files to staging area in all enabled repos
"

# SCRIPTS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
# if [[ $U_NO_GIT_SHORTCUTS -ne 1 ]]; then
#     source $SCRIPTS_DIR/git_shortcuts.sh
# fi

while getopts ':hi:' option; do
  case "$option" in
    h) echo "$usage"
       exit
       ;;
    :) printf "missing argument for -%s\n" "$OPTARG" >&2
       echo "$usage" >&2
       exit 1
       ;;
   \?) printf "illegal option: -%s\n" "$OPTARG" >&2
       echo "$usage" >&2
       exit 1
       ;;
  esac
done
shift $((OPTIND - 1))

SCRIPTS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd)"
source $SCRIPTS_DIR/helpers.sh

repos=$(list_enabled)
for repo in $(echo $repos); do
    if [[ $? -eq 0 ]];then
        print_batch_header "Doing '${@}' in $repo"
        REPO_LOCATION=$U_REPOS_ROOT/$repo
        if [[ -e $REPO_LOCATION ]];then
            cd $REPO_LOCATION
            # this expands the command and args
            ${@}
        else
            echo "$repo doesn't exist on this machine. Skipping."
        fi

    fi
done

cd -
